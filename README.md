cups container image
=========================

Description
-----------

CUPS printing system provides a portable printing layer for 
UNIX® operating systems. It has been developed by Apple Inc. 
to promote a standard printing solution for all UNIX vendors and users. 
CUPS provides the System V and Berkeley command-line interfaces.

Usage
-----

```
$ podman run -d --name cups -p 6631:6631 rhel8/cups
```

Configuration
-----

Simplest way to configure cups is to use this image as a base and
build container with your own configuration file.